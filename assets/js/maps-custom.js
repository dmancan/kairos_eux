// When the window has finished loading create our google map below
google.maps.event.addDomListener(window, 'load', init);

function init() {
    // Basic options for a simple Google Map
    // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
    var mapOptions = {
        // How zoomed in you want the map to start at (always required)
        zoom: Number(kairos_script_vars.map_zoom),

        // The latitude and longitude to center the map (always required)
        center: new google.maps.LatLng(kairos_script_vars.map_lat,kairos_script_vars.map_lng),

        // Remove map type and streeview
        mapTypeControl: false,
        streetViewControl: false,

        // How you would like to style the map.
        // This is where you would paste any style found on Snazzy Maps.
        styles: [{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"on"},{"lightness":33}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2e5d4"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#c5dac6"}]},{"featureType":"poi.park","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":20}]},{"featureType":"road","elementType":"all","stylers":[{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#c5c6c6"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#e4d7c6"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#fbfaf7"}]},{"featureType":"water","elementType":"all","stylers":[{"visibility":"on"},{"color":"#acbcc9"}]}]
    };

    // Get the HTML DOM element that will contain your map
    // We are using a div with id="map" seen below in the <body>
    var mapElement = document.getElementById('partners-map');

    // Create the Google Map using our element and options defined above
    var map = new google.maps.Map(mapElement, mapOptions);

    // Let's also add a markers while we're at it
    for (var i = kairos_script_vars.partners.length - 1; i >= 0; i--) {
        var myLatLng = new google.maps.LatLng(kairos_script_vars.partners[i].coordinates['lat'],kairos_script_vars.partners[i].coordinates['lng']);
        var ppMarkup = '<div id="cm-map-pin-'+ kairos_script_vars.partners[i].id +'" class="cm-map-pin"><strong>'+ kairos_script_vars.partners[i].title +'</strong><br><em>'+ kairos_script_vars.partners[i].infowindow +'</em></div>';
        var ppMarkerID = kairos_script_vars.partners[i].id;
        if( kairos_script_vars.partners[i].partnership == "offices" || kairos_script_vars.partners[i].partnership == "regional" || kairos_script_vars.partners[i].partnership == "country" ){
            var markerIcon = kairos_script_vars.theme_location + "/assets/images/google-maps/"+ kairos_script_vars.partners[i].partnership+"Pointer.png";
        } else if( kairos_script_vars.pointer != "" ){
            var markerIcon = kairos_script_vars.theme_location + "/assets/images/google-maps/networkPointer.png";
        } else{
            var markerIcon = kairos_script_vars.theme_location + "/assets/images/google-maps/regularPointer.png";
        }
        var marker = createMarker( map, myLatLng, ppMarkup, ppMarkerID, markerIcon );
        //console.log(kairos_script_vars.partners[i]);
    };

    //infowindow Builder
    var infowindow = new google.maps.InfoWindow({
        size: new google.maps.Size(553,315),
        maxWidth: 553
    });

    //Marker Builder
    var selectedMarker = "";
    function createMarker( ppMap, latlng, html, ppMapID, markerIcon ) {
        var contentString = html;
        var marker = new google.maps.Marker({
            position: latlng,
            map: ppMap,
            icon: markerIcon,
            draggable: false,
            zIndex: Math.round(latlng.lat()*-100000)<<5
        });

        google.maps.event.addListener(marker, 'click', function() {
            selectedMarker = marker;
            infowindow.setContent(contentString);
            infowindow.open( ppMap, marker );
        });


    }

}
