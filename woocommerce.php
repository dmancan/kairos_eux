<?php  get_header(); ?>

	<section class="container">
		<div class="row">
			<div class="col-md-8">
				<?php woocommerce_content(); ?>
			</div>
			<?php get_template_part( 'parts/sidebar'); ?>
		</div>
	</section>
	
<?php get_footer(); ?>