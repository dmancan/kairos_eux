<?php /* Template Name: Get Involved */
get_header(); ?>

	<?php
		if (have_posts()) :
			while (have_posts()) : the_post();
		?>
		<?php get_template_part( 'parts/hero'); ?>
		<section class="container">
			<div class="row">
				<div class="col-md-8">
					<h1 <?php if(get_field('_custom_color') == 'true') echo 'style="color:'.get_field('select_color').'"'?>><?php echo (get_post_meta($post->ID, '_custom_title', true) ? get_post_meta($post->ID, '_custom_title', true) : $post->post_title); ?></h1>
					<?php if(get_field('_page_intro')) echo '<div class="page-intro">'.get_field('_page_intro', false, false).'</div>';?>
					
					<?php the_content(); ?>
					
					<hr>
					<?php 
						$rowCounter = 0;
						if(get_field('_content_boxes')):?>
						<div class="row">
							<?php while(has_sub_field('_content_boxes')):?>
								<div class="col-sm-6">
									<?php 
										echo get_sub_field('_content_boxes_content');
										$rowCounter++;
									?>
								</div>
							<?php 
								if ($rowCounter == 2): ?>
								<div class="clearfix hidden-xs"></div>
								<?php 
									$rowCounter = 0;
									endif;
							endwhile;?>
						</div>
					<?php endif;?>

					<?php
							endwhile;
						else:
							get_template_part('404');
						endif;
					?>
				</div>
				<?php get_template_part( 'parts/sidebar'); ?>
			</div>
		</section>
<?php get_footer(); ?>