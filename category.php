<?php get_header(); ?>
	<section class="container">
		<div class="row">
			<div class="col-md-8">
				<h1><?php single_cat_title(); ?></h1>
			
				<?php if (have_posts()) : ?>
					<?php while (have_posts()) : the_post(); ?>

							<?php get_template_part( 'parts/content-post' ); ?>
							
						<?php endwhile; ?>
				
				<div class="add-more"><?php next_posts_link('Get More Posts') ?></div>
				<?php endif; wp_reset_postdata(); ?>
			</div>
			<div class="col-md-3 col-md-offset-1">
				<div id="sidebar" class="blog">
				
				<?php
					$term = get_term( get_cat_id( single_cat_title("",false) ), 'category' );
				?>
				<h4><?php echo get_field('category_short_title', $term); ?></h4>
				<p><?php 
					$desc = strip_tags(category_description());
					echo $desc
				;?></p> 
				
				<hr>
				<?php dynamic_sidebar('category-sidebar'); ?>
				</div>
			</div>
		</div>
	</div>
<?php get_footer(); ?>