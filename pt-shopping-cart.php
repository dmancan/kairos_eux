<?php /* Template Name: Shopping Cart */
get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<section class="container cart">
		<div class="row">
			<div class="col-md-12">
				<h1><?php the_title(); ?></h1>
				<?php the_content(); ?>
			</div>
		</div>
	</section>
<?php endwhile; endif; ?>
<?php get_footer(); ?>