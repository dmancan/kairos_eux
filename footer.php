</main>

<footer id="footer">
	<nav class="navbar navbar-inverse">
		<div class="container">
			<div class="nav navbar-nav navbar-left">
			<?php if(function_exists('bootstrap_breadcrumbs')) bootstrap_breadcrumbs(); ?>
			</div>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="#top">Top <span class="icon-up-open-big"></span></a></li>
			</ul>
		</div>
	</nav>
	<section class="container">
		<div class="row">
			<div class="col-xs-6 col-sm-3">
		
				<?php dynamic_sidebar( 'Footer Right Widgets Area' ); ?>
			</div>
			<div class="col-xs-6 col-sm-3">	
				<?php dynamic_sidebar( 'Footer Left Widgets Area' ); ?>
			</div>
			<div class="clearfix visible-xs"></div>
			<div class="col-sm-3 col-sm-offset-1">
				<br class="visible-xs">
				<br class="visible-xs">
				<?php if(get_field('_additional_info2','options')):?>
					<div class="additional">
						<?php while(has_sub_field('_additional_info2','options')):?>
							<?php echo get_sub_field('_additional_info_item2');?>
						<?php endwhile;?>
					</div>
				<?php endif;?>
			</div>
			<div class="col-sm-2 text-center hidden-xs hidden-sm">
				<img src="<?php echo get_template_directory_uri()?>/assets/images/logo.01.png" alt="Kairos" />
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<?php echo '<p>'.get_field('_footer_info','options').'</p>';?>
			</div>
			<div class="col-sm-3 col-sm-offset-1">
				<p><a href="tel:18774038933" style="white-space: normal;">Toll free: 1-877-403-8933</a><br>
				<a href="mailto:info@kairoscanada.org">info@kairoscanada.org</a>
			</div>
			<div class="col-sm-2 text-center hidden-xs">
				
					<p><a href="<?php the_field('donate2', 'options'); ?>" class="btn btn-warning"><?php _e( "DONATE NOW", "kairos" ); ?></a></p>
			</div>
		</div>
	</section>
</footer>
	
	
<!-- //viewport -->
</div>
<?php wp_footer(); ?>
</body>
</html>