<?php /* Template Name: Landing Page (top menu) */
get_header(); ?>
	<?php if(have_posts()): while(have_posts()): the_post();?>
	
	<section class="hero">
		<section class="container">
			<div class="row">
				<div class="col-md-8">
					<h1 <?php if(get_field('_custom_color') == 'true') echo 'style="color:'.get_field('select_color').'"'?>><span><?php echo get_the_title($post->post_parent);?></span>
					<?php the_title(); ?></h1>
					<h2 <?php if(get_field('_custom_color') == 'true') echo 'style="color:'.get_field('select_color').'"'?>><?php if(get_field('_page_intro')) echo get_field('_page_intro', false, false); ?></h2>
				</div>
				<div class="col-md-3 col-md-offset-1">
					<?php dynamic_sidebar('interior-page-sidebar'); ?>
				</div>
			</div>
		</section>
		
		<?php if(has_post_thumbnail()):?>
			<?php
				$thumb_id = get_post_thumbnail_id();
				$thumb_url = wp_get_attachment_image_src($thumb_id,'full', true);
			?>
			<div class="bg" style="background-image: url(<?php echo $thumb_url[0];?>);"></div>
			<?php else: ?>
			<div class="bg"></div>
		<?php endif;?>
	</section>
	
	<section class="container">
			<div class="row">
				<div class="col-md-8">
					<article>
						<?php the_content(); ?>
					</article>
				</div>
				<div class="col-md-3 col-md-offset-1">
					<?php if( have_rows('videos2') || is_active_sidebar('interior-page-products-sidebar')) : ?>
						<?php while( have_rows('videos2') ) : the_row(); ?>
							<?php the_sub_field('video2'); ?>
							<hr>
						<?php endwhile; ?>
						<?php dynamic_sidebar('interior-page-products-sidebar'); ?>
					<?php endif; ?>
				</div>
			</div>
			<?php get_template_part( 'parts/related-posts'); ?>
	</section>
<?php endwhile; endif; ?>
<?php get_footer(); ?>