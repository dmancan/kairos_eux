<?php global $curr_page; ?>

<?php if( is_page() ) : ?>
<div id="sidebar">
	<?php dynamic_sidebar('page-sidebar'); ?>
</div>

<?php elseif(is_post_type_archive( 'product' ) || is_singular( 'product' )) : ?>
<div id="sidebar">
	<?php dynamic_sidebar('shop-sidebar'); ?>
</div>

<?php elseif( is_post_type_archive( 'post' )|| is_home() || is_tag() || is_date() ) : 
// Post Archives, Posts, and Main Blog roll ?>
<div id="sidebar" class="blog">
	<?php dynamic_sidebar('main-sidebar'); ?>
</div>

<?php elseif( is_singular( 'post' ) ) : 
// Single Post ?>
<div id="sidebar" class="blog">
	<?php dynamic_sidebar('post-sidebar'); ?>
</div>

<?php elseif( is_singular( 'people' ) ) : 
// Single Bio ?>
<div id="sidebar">
	<?php dynamic_sidebar('people-sidebar'); ?>
</div>


<?php elseif( is_archive()) : 
// Products Archive ?>
<div id="sidebar">
	<?php dynamic_sidebar('shop-sidebar'); ?>
</div>

<?php else: ?>
<div id="sidebar" class="blog">

</div>
<?php endif; ?>