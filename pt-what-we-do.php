<?php
/**
 * Template Name: What We Do
 */
get_header(); ?>

	<?php
		if (have_posts()) :
			while (have_posts()) : the_post();
		?>
		<?php get_template_part( 'parts/hero'); ?>
	
		<section class="container">
			<div class="row">
				<div class="col-md-8">
					<h1 <?php if(get_field('_custom_color') == 'true') echo 'style="color:'.get_field('select_color').'"'?>><?php echo (get_post_meta($post->ID, '_custom_title', true) ? get_post_meta($post->ID, '_custom_title', true) : $post->post_title); ?></h1>
					<?php if(get_field('_page_intro')) echo '<div class="page-intro">'.get_field('_page_intro', false, false).'</div>';?>
					<?php the_content(); ?>
				</div>
				<?php get_template_part( 'parts/sidebar'); ?>
			</div>
			<div class="row">
				<?php if( have_rows('testimonials') ) : ?>
					<?php while( have_rows('testimonials') ) : the_row(); ?>
						<div class="col-md-3">
							<hr>
							<h3><?php the_sub_field('title'); ?></h3>
							<blockquote><?php the_sub_field('quote'); ?></blockquote>
							<p><em><?php the_sub_field('author'); ?></em></p>
						</div>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</section>
		<hr>
		<section class="container">
			<div class="row">
			<?php if(get_field('content') || get_field('title')):?>
				<div class="col-md-8">
					<h1><?php if(get_field('title')) echo get_field('title', false, false)?></h1>
					<p><?php if(get_field('content')) echo get_field('content', false, false);?></p>
				</div>
			<?php endif;?>
			</div>
			
			<?php get_template_part( 'parts/areas-of-focus'); ?>
					
		</section>
		<?php endwhile; endif; ?>
<?php get_footer(); ?>