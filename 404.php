<?php get_header(); ?>
	<div class="container">
		<header class="text-center well">
			<h1>Search Our Website</h1>
			<?php get_search_form(); ?>
		</header>
		<section>
			<h1>Error 404: Page Not Found</h1>
		

				<h2>You may not be able to find the page or file because of:</h2>
				<ol>
					<li>An <strong>out-of-date bookmark/favorite.</strong></li>
					<li>A search engine that has an <strong>out-of-date listing for this site.</strong></li>
					<li>A <strong>mis-typed address</strong>.</li>
				</ol>
		</section>

	</div>
<?php get_footer(); ?>