<?php
/*
 * Template Name: Network
*/

get_header();
?>

	<?php
		if (have_posts()) :
			while (have_posts()) : the_post();
		?>
		
		<?php get_template_part( 'parts/hero'); ?>

		<section class="container">
			<div class="row">
				<div class="col-md-8">
					<h1 <?php if(get_field('_custom_color') == 'true') echo 'style="color:'.get_field('select_color').'"'?>><?php echo (get_post_meta($post->ID, '_custom_title', true) ? get_post_meta($post->ID, '_custom_title', true) : $post->post_title); ?></h1>
					<?php if(get_field('_page_intro')) echo '<div class="page-intro">'.get_field('_page_intro').'</div>';?>
					
					<div id="partners-map"></div>
					
					<article><?php  the_content(); ?></article>
					
					<?php
						$canadia_network = get_term_by( 'slug', 'canadian-network', 'partnership' );
						$provinces = get_terms( 'partnership', array(
								'orderby'    => 'menu_order',
								'hide_empty' => 0,
								'parent'     => $canadia_network->term_id
							)
						);
					?>
					<?php if ( ! empty( $provinces ) && ! is_wp_error( $provinces ) ): ?>

						<div role="tabpanel">

							<ul class="nav nav-tabs" role="tablist">
								<?php 
									$active = true;
									foreach ($provinces as $province) : ?>
									<li 
										<?php if ($active):?>
										class="active"
										<?php 
										$active = false;
										endif; ?>
									>
										<a href="#<?php echo $province->slug; ?>" data-toggle="tab"><?php echo $province->name; ?></a>
									</li>
								<?php endforeach; ?>
							</ul>
							
							<div class="tab-content">
							<?php 
								$active = true;
								foreach ($provinces as $province) : ?>
								<div id="<?php echo $province->slug; ?>" 
									class="tab-pane 
									<?php if ($active):?>
										 active
										<?php 
										$active = false;
										endif; ?>
									">

									<h1><?php _e( "Regional Contact", "kairos" ); ?></h1>
									<?php echo wpautop( $province->description ); ?>

									<h1><?php _e( "Network", "kairos" ); ?></h1>
									<?php $args = array(
										'post_type' => 'partners',
										'posts_per_page' => 1000,
										'orderby' => 'menu_order',
										'order' => 'ASC',
										'tax_query' => array(
											array(
												'taxonomy' => 'partnership',
												'field'    => 'slug',
												'terms'    => $province->slug,
											),
										),
									);
									$query = new WP_Query( $args );
		
									if ( $query->have_posts() ) : ?>
									<?php while ( $query->have_posts() ) : $query->the_post(); ?>
										<h5><?php the_title(); ?> &mdash; <?php the_field('specific_location'); ?></h5>
										<p><?php the_field('_contact_name'); echo ' &mdash; ' . antispambot( get_field('contact_email') ); ?></p>
										<?php the_content(); ?>
										<hr>
									<?php endwhile; endif; ?>

								</div>
							<?php endforeach; ?>
							</div>
						</div>

					<?php endif; //if $provinces ?>

				</div>
				<?php get_template_part( 'parts/sidebar'); ?>
			</div>
		</section>
		<?php endwhile; endif; ?>
<?php get_footer(); ?>