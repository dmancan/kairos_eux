<?php get_header(); 
	

	
?>
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<section class="container">
			<div class="row">
				<div class="col-md-8">
					<article>
						<?php $category = get_the_category();?>
						
						
						<?php if(get_post_type() == 'post') : ?>
							<span><time><?php the_time( 'M. jS, Y' ); ?></time></span>
						<?php endif; ?>
						
						
						
						
						
						<h1><?php the_title(); ?></h1>
						
						<?php if(get_field('intro')) echo '<div class="page-intro">'.get_field('intro').'</div>';?>
						<hr>
						<?php if( has_post_thumbnail() ){ ?>
							<div class="pull-right hidden-xs" style="margin-left: 20px;">
								<?php the_post_thumbnail( 'medium',array( 'class'	=> "") )?>
									
							</div>
							<div class="visible-xs">
								<?php the_post_thumbnail( 'medium',array( 'class'	=> "img-responsive") )?>
							</div>
						<?php ; } ?>
						
						<?php if(get_field('position')) echo '<p><span class="position">'.get_field('position').'</span><br />';?>
						<?php if(get_field('email_address')) echo '<span class="email-address"><a href="mailto:'.get_field('email_address').'">'.get_field('email_address').'</a></span><br />';?>
						<?php if(get_field('phone_number')) echo '<span class="phone-number"><a href="tel:'.get_field('phone_number').'">'.get_field('phone_number').'</a></span> ';?>
						<?php if(get_field('extension')) echo '<span class="phone-extension">Ext. '.get_field('extension').'</span></p>';?>
						
						
						
						<?php the_content(); ?>
					</article>
					<hr>
					
					<?php if(get_post_type() == 'post') : ?>
						Filed in: <?php the_category(', '); ?> <br /><br />
						Tags: <?php the_tags(' ',', ',' '); ?><br /><br />
					<?php endif; ?>
				</div>
				<div class="col-md-3 col-md-offset-1">
					<?php get_sidebar(); ?>
					
				</div>
			</div>
			<?php echo do_shortcode('[ssba multisite]'); ?>
		</section>
		<section class="container">
			<?php get_template_part( 'parts/related-posts'); ?>
		</section>
	<?php endwhile; endif; ?>
<?php get_footer(); ?>