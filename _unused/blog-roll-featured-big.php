<?php get_header(); ?>
	
	<?php $featureCounter = 0; if (have_posts()) : ?>
				
		<?php while (have_posts()) : the_post(); ?>
			<?php if( $featureCounter <= 0 & !is_paged() ) : ?>
				<section class="hero">
					<section class="container">
						<div class="row">
							<div class="col-sm-6 col-md-6">
								<h1><span>Featured Post</span>
								<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
								<h2><?php the_excerpt(); ?></h2>
							
								<p><?php the_time('M. jS, Y'); ?>  &mdash; <?php the_category( ', ' ); ?></p>
							</div>
							<div class="col-sm-6 col-md-6">
								<?php if( has_post_thumbnail() ): ?>
									
									<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('blog-featured',array( 'class'	=> "img-responsive"));?></a>
									
								<?php endif; ?>
							</div>

						</div>
					</section>
					<div class="bg"></div>
				</section>
				<?php $featureCounter++; ?>
				<section class="container">
					<div class="row">
						<div class="col-md-8">
							<h1>Latest Posts</h1>
							<hr>
							<div class="infinite-posts">
			<?php else : ?>	
						
							<?php get_template_part( 'parts/content-post' ); ?>
						
	
			<?php endif; ?>
		<?php endwhile; ?>
							</div>
							<div class="infinite-btn"><?php next_posts_link('Load More Posts') ?></div>
		<?php endif; wp_reset_postdata(); ?>
						</div>
						<?php get_template_part( 'parts/sidebar'); ?>
					</div>
				</section>
<?php get_footer(); ?>