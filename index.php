<?php get_header(); ?>
	
	<?php $featureCounter = 0; if (have_posts()) : ?>
				
		<?php while (have_posts()) : the_post(); ?>
			<?php if( $featureCounter <= 0 & !is_paged() ) : ?>
				
				<section class="container">
					<div class="row">
						<div class="col-md-8">
								<div class="row">
									<div class="col-md-6">
									<?php if( has_post_thumbnail() ): ?>								
									<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('blog-featured',array( 'class'	=> "img-responsive"));?></a>
									<?php endif; ?>
									</div>
									
									<div class="col-md-6">
										<h2 class="featured-post"><span>Featured Post</span></h2>
										<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
										
									</div>
								</div><!--end row-->
								
								<div class="row">
									<div class="col-md-12">
										<p><?php the_excerpt(); ?></p>
										<p><?php the_time('F j, Y'); ?>  &mdash; <?php the_category( ', ' ); ?></p>
									</div>
								</div>
								<hr>
								<?php $featureCounter++; ?>
							
								<div class="row">
									<div class="infinite-posts">
			<?php else : ?>	
						
										<?php get_template_part( 'parts/content-post' ); ?>
						
			<?php endif; ?>
		<?php endwhile; ?>
									</div>
									<div class="infinite-btn">
										<?php next_posts_link('Load More Posts') ?>
									</div>
	<?php endif; wp_reset_postdata(); ?>
									
								</div>
							</div><!--end 8-->
					
						
					
			

						<?php get_template_part( 'parts/sidebar'); ?>
						
					</div> <!--end row-->
				</section>
			
<?php get_footer(); ?>