<?php
/*
 * Template Name: Search
*/

get_header();
?>
	<section class="container">
		<header class="text-center well">
			<h1><?php _e( "Search KAIROS", "kairos" ); ?></h1>
			<?php //if( !$s ) : ?>
				<?php echo get_search_form(); ?>
			<?php //endif; ?>
		</header>
		<div class="row">
			<div class="col-md-8">		
			<?php if (have_posts() && $s) : ?>
				<h2>Your search topic <span class="alert alert-info"><?php echo esc_html($s); ?></span> returned the following articles:</h2>
				<hr>
				<?php while (have_posts()) : the_post(); ?>
					<?php get_template_part( 'parts/content-post' ); ?>
				<?php endwhile; ?>
				
				<?php
					$next_link = get_next_posts_link(__('Load More'));
					if( $next_link ) :
				?>
				<?php echo $next_link; ?>
			<?php endif; ?>
			<?php elseif( $s ) : ?>
				<h2>Sorry, but your search term <strong><?php echo esc_html($s); ?></strong> returned <strong>0</strong> results.</h2>
				
			<?php else: ?>
<!-- 				<h2>Looking for something? Try searching our site.</h2> -->
			<?php endif; ?>
			</div>
			<div class="col-md-3 col-md-offset-1">
				<?php dynamic_sidebar('main-sidebar'); ?>
			</div>
		</div>
	</section>
<?php get_footer(); ?>