<?php /* Template Name: Home  */ get_header(); ?>
	<section class="container">
		<?php //if(get_field('_news_title')) echo '<h1>'.get_field('_news_title').'</h1><br>'?>
		<h1><?php if(get_field('_news_title')) echo get_field('_news_title') ?> | <a href="<?php echo get_permalink( get_option('page_for_posts') ); ?>"><?php _e( "More", "kairos" ); ?></a></h1>

		<?php if(get_field('_news_box')) echo get_field('_news_box');?>
		<hr>
			<?php
			  // Set the default amount of posts we want and ensuring we don't exceed 8 posts
			 $posts_per_page = 8;
			 // Get the sticky posts array
			 $sticky_posts = get_option( 'sticky_posts' );
			 // Count the number of posts in the array
			 $sticky_count = count($sticky_posts);
			 // Adjust the posts_per_page minusing out the sticky posts.
			 $posts_per_page = $posts_per_page - $sticky_count;
			
			$args = array(
				//Type & Status Parameters
				'post_type'   => 'post',
				'posts_per_page' => $posts_per_page
			);

			$query = new WP_Query( $args );
			$rowCounter = 0;
			?>
			<?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
				
				<?php if ($rowCounter == 0):?>
					<div class="row">
				<?php endif; ?>
				
					<div class="col-sm-6 col-md-3">
						<?php get_template_part( 'parts/content-post' ); ?>
					</div>
				<?php 
					$rowCounter++; // row counter... every 4 (every 2nd on smaller screens...)
					if ($rowCounter == 2): ?>
					<div class="clearfix visible-xs visible-sm"></div>
				<?php endif;
					if (($rowCounter % 4) == 0):?>
					</div><!--end row-->
					<?php $rowCounter = 0; ?>
				<?php endif; ?>
			<?php endwhile; endif; wp_reset_postdata(); ?>	
	
		<br><br>
		<?php if(get_field('_info_box') || get_field('_info_title')):?>
			<div class="row">
				<div class="col-md-8">
					<h1><?php if(get_field('_info_title')) echo get_field('_info_title') ?></h1>
					<?php if(get_field('_info_box')) echo '<p>'.get_field('_info_box', false, false).'</p>';?>
				</div>
			</div>
			<hr>
		<?php endif;?>
		
		<?php get_template_part( 'parts/areas-of-focus'); ?>
		
	</section>
<?php get_footer(); ?>