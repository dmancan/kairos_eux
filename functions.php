<?php

// Core
include('functions/core.php');

// Site Specific
include('functions/custom.php');

// Register custom navigation walker
require_once('functions/wp_bootstrap_navwalker.php');

// Register custom breadcrumbs
require_once('functions/wp_bootstrap_breadcrumbs.php');

?>