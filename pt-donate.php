<?php /* Template Name: Donate */
get_header(); ?>

	<?php
		if (have_posts()) :
			while (have_posts()) : the_post();
		?>
		<section class="hero">
			<section class="container">
				<h1><?php echo (get_post_meta($post->ID, '_custom_title', true) ? get_post_meta($post->ID, '_custom_title', true) : $post->post_title); ?></h1>
				<div class="row">
					<div class="col-md-8">
						<h2><?php if(get_field('_donate_intro')) echo get_field('_donate_intro', false, false); ?></h2>
					</div>
					<div class="col-md-4">
						<p><?php if(get_field('_donate_url_intro')) echo '<a class="btn btn-warning btn-lg" href="'.get_field('_donate_url_intro').'">'.get_field('_donate_url_intro_name').'</a>'?></p>
					</div>
				</div>
			</section>
			<div class="bg"></div>
		</section>


		<section class="container">
			<div class="row">
				<div class="col-md-8">
					<article>
						<?php the_content(); ?>
					</article>
				</div>
			</div>
			<hr>
			<?php if(get_field('donate_boxes2')):?>
				
				<?php 
					$rowCounter = 0;
					while(has_sub_field('donate_boxes2')):?>
					<?php if ($rowCounter == 0):?>
					<div class="row">
					<?php endif; ?>
						<div class="col-md-4">
								<?php if(get_sub_field('main_d_content2')) echo '<div class="main-content">'.get_sub_field('main_d_content2').'</div>'?>
								<?php if(get_sub_field('call_d_content2') || get_sub_field('call_url2')):?>
									<div class="action">
										<?php echo get_sub_field('call_d_content2');?>
										<?php if(get_sub_field('call_url2')):?>
											<p><a class="btn btn-warning" href="<?php echo get_sub_field('call_url2');?>"><?php echo get_sub_field('call_url_name2');?></a></p>
										<?php endif;?>
									</div>
								<?php endif;?>
						</div>
					<?php 
					$rowCounter++; // row counter... every 3
					if (($rowCounter % 3) == 0):?>
					</div>
					<?php $rowCounter = 0; ?>
					<?php endif; ?>
				<?php endwhile;?>
				<?php 
				// close row
				if ($rowCounter < 3):?>
				</div>
				<?php $rowCounter = 0; ?>
				<?php endif; ?>
			<?php endif;?>
		</section>
		<?php
				endwhile;
			else:
				get_template_part('404');
			endif;
		?>
<?php get_footer(); ?>