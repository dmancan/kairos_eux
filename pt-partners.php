<?php
/*
 * Template Name: Partners
*/

get_header();
?>

	<?php
		if (have_posts()) :
			while (have_posts()) : the_post();
		?>
		
		<?php get_template_part( 'parts/hero'); ?>
		
		<section class="container">
			<div class="row">
				<div class="col-md-8">
					
						<h1 <?php if(get_field('_custom_color') == 'true') echo 'style="color:'.get_field('select_color').'"'?>><?php echo (get_post_meta($post->ID, '_custom_title', true) ? get_post_meta($post->ID, '_custom_title', true) : $post->post_title); ?></h1>
						<?php if(get_field('_page_intro')) echo '<div class="page-intro">'.get_field('_page_intro').'</div>';?>
						
						<div id="partners-map"></div>
						
						<article><?php  the_content(); ?></article>
						
						<?php
							$continents = get_terms( 'continent', array(
								'parent' => 0,
								'orderby'    => 'menu_order',
								'hide_empty' => 0,
							)	);
							$partnerships = get_terms( 'partnership', array(
								'parent' => 0,
								'orderby'    => 'menu_order',
								'hide_empty' => 0,
							)	);
						?>
						<?php if ( ! empty( $continents ) && ! is_wp_error( $continents ) ): ?>

						<div role="tabpanel">

							<ul class="nav nav-tabs" role="tablist">
								<?php 
									$active = true;
									foreach ($continents as $continent) : ?>
									<li 
										<?php if ($active):?>
										class="active"
										<?php 
										$active = false;
										endif; ?>
									>
										<a href="#<?php echo $continent->slug; ?>" data-toggle="tab"><?php echo $continent->name; ?></a>
									</li>
								<?php endforeach; ?>
							</ul>
							<div class="tab-content">
								<?php 
								$active = true;
								foreach ($continents as $continent) : ?>
								<div id="<?php echo $continent->slug; ?>" 
									class="tab-pane 
									<?php if ($active):?>
										 active
										<?php 
										$active = false;
										endif; ?>
									">

									<?php foreach ($partnerships as $partnership) : ?>
									<?php $args = array(
										'post_type' => 'partners',
										'posts_per_page' => 1000,
										'orderby' => 'menu_order',
										'order' => 'ASC',
										'tax_query' => array(
											'relation' => 'AND',
											array(
												'taxonomy' => 'continent',
												'field'    => 'slug',
												'terms'    => $continent->slug,
											),
											array(
												'taxonomy' => 'partnership',
												'field'    => 'slug',
												'terms'    => $partnership->slug,
											),
										),
									);
									$query = new WP_Query( $args );
	
									if ( $query->have_posts() ) : ?>
										<h1><?php echo $partnership->name; ?></h1>
										<?php while ( $query->have_posts() ) : $query->the_post(); ?>
											<hr>
											<?php the_title( '<h2>', '</h2>' ); ?>
											<h5><?php the_field('specific_location'); ?></h5>
											<?php the_content(); ?>
											
										<?php endwhile; endif; ?>
									<?php endforeach; ?>

								</div>
							<?php endforeach; ?>

						</div>
					</div>

					<?php endif; //if $continents ?>

				</div>
				
				<?php get_template_part( 'parts/sidebar'); ?>
				
			</div>
		</section>
	<?php endwhile; endif; ?>
<?php get_footer(); ?>