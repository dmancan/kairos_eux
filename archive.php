<?php get_header(); ?>
	<section class="container">
		<div class="row">
			<div class="col-md-8">
				<?php if(is_category()){ ?>
				<h1>Archive for the "<?php single_cat_title(); ?>" Category</h1>
				<?php } elseif(is_tag()) { ?>
				<h1>Posts Tagged "<?php single_tag_title(); ?>"</h1>
				<?php } elseif (is_day()) { ?>
				<h1>Archive for <?php the_time('F jS, Y'); ?></h1>
				<?php } elseif (is_month()) { ?>
				<h1>Archive for <?php the_time('F, Y'); ?></h1>
				<?php } elseif (is_year()) { ?>
				<h1>Archive for <?php the_time('Y'); ?></h1>
				<?php } elseif (is_author()) { ?>
				<h1>Author Archive</h1>
				<?php } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
				<h1>Blog Archives</h1>
				<?php } ?>
	
				<?php if (have_posts()) : ?>
				<div class="posts-list">
					<?php while (have_posts()) : the_post(); ?>
							<?php get_template_part( 'parts/content-post' ); ?>
					<?php endwhile; ?>
				</div>
				<div class="add-more"><?php next_posts_link('Load More') ?></div>
				<?php endif; wp_reset_postdata(); ?>
			</div>
			<?php get_template_part( 'parts/sidebar'); ?>
		</div>
	</section>
<?php get_footer(); ?>