<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<title><?php wp_title('|',true,'right'); ?></title>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="robots" content="noodp">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon.ico" rel="shortcut icon">
	<?php wp_head(); ?>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body <?php body_class(); ?>>
<?php
	global $curr_page;
	$curr_page = $wp_query->get_queried_object();
	$lang_fr = "";
	$lang_en = "active";
	if( is_page_template('pt-french.php') ){
		$lang_fr = "active";
		$lang_en = "";
	}
?>
<div class="viewport" id="top">
<section class="global-nav">
	<nav class="navbar navbar-inverse hidden-xs">
		<div class="container">
			<div class="">
				<ul class="nav navbar-nav navbar-left hidden-xs">
			  		<li><a href="/" style="padding-left: 0;">Faithful Action For Justice</a></li>
		  		</ul>
				<ul class="nav navbar-nav navbar-right">
		  			<li><a href="<?php the_field('_flickr2', 'options'); ?>"><span class="icon-flickr"></span></a></li>
		  			<li><a href="<?php the_field('_tube2', 'options'); ?>"><span class="icon-youtube"></span></a></li>
		  			<li><a href="<?php the_field('_twitter2', 'options'); ?>"><span class="icon-twitter"></span></a></li>
		  			<li><a href="<?php the_field('_facebook2', 'options'); ?>"><span class="icon-facebook"></span></a></li>
		  		</ul>
			</div>
		</div>
	</nav>
	<nav class="navbar navbar-default">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navigation">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand visible-xs" href="<?php echo home_url();?>">
					Kairos Canada
				</a>
				<a class="navbar-brand hidden-xs" href="<?php echo home_url();?>">
					<img src="<?php echo get_template_directory_uri();?>/assets/images/logo.01.png" alt="<?php bloginfo('name'); ?>" />
	  			</a>
	  		</div>
	  		<div class="collapse navbar-collapse" id="navigation">
		  		<?php /* Primary navigation */
				wp_nav_menu( array(
				  'menu' => 'Main Menu',
				  'depth' => 2,
				  'container' => false,
				  'menu_class' => 'nav navbar-nav navbar-right',
				  //Process nav menu using our custom nav walker
				  'walker' => new wp_bootstrap_navwalker())
				);
				?>
				<div class="clearfix hidden-xs"></div>
				<?php /* Primary navigation */
				wp_nav_menu( array(
				  'menu' => 'Secondary Main Menu',
				  'depth' => 2,
				  'container' => false,
				  'menu_class' => 'nav navbar-nav navbar-right jk-tweak',
				  //Process nav menu using our custom nav walker
				  'walker' => new wp_bootstrap_navwalker())
				);
				?>
	  		</div>
		</div>
	</nav>
	<div class="decor hidden-xs">
		<div class="bg hidden-xs"></div>
	</div>
</section>
<?php if(is_page_template('pt-home.php')):?>
	<?php if(get_field('_home_promo_boxes')):?>
		<section class="hero feature">
			<?php get_template_part( 'parts/carousel'); ?>
			<div class="bg"></div>
		</section>
		<section class="hero promo">
			<div class="container">
				<div class="row">
					<?php while(has_sub_field('_home_promo_boxes')):?>
					<div class="col-sm-3">
						<?php if(get_sub_field('_home_promo_title')) echo '<h2>'.get_sub_field('_home_promo_title').'</h2>'?>
						<?php if(get_sub_field('_home_promo_text')) echo '<p>'.get_sub_field('_home_promo_text').'</p>'?>
						<?php if(get_sub_field('_home_btn_url')) echo '<p><a href="'.get_sub_field('_home_btn_url').'" class="btn btn-warning">'.get_sub_field('_home_btn_name').'</a></p><hr class="visible-xs">'?>
					</div>
					<?php endwhile;?>
				</div>
			</div>
			<div class="bg"></div>
		</section>
		
	<?php endif;?>

<?php endif;?>

<main id="main">