<?php

// Custom Enqueues
function custom_scripts_styles() {
	if( is_page_template( 'pt-partners.php' ) || is_page_template( 'pt-network.php' )  ) {
		wp_enqueue_script( 'maps-api', 'https://maps.googleapis.com/maps/api/js?sensor=false', array(), true );
		wp_enqueue_script( 'maps-custom', get_template_directory_uri() . '/assets/js/maps-custom.js', array('maps-api'), true );
		if( is_page_template( 'pt-partners.php' ) ) :
			wp_localize_script('maps-custom', 'kairos_script_vars', array(
					'theme_location' => get_template_directory_uri(),
					'partners' => get_partners( array( 'canadian-network' ), true, "NOT IN" ),
					'map_lat' => 38.636674,
					'map_lng' => 0.71113,
					'map_zoom' => 1,
					'pointer' => '',
				)
			);
		elseif( is_page_template( 'pt-network.php' ) ) :
			wp_localize_script('maps-custom', 'kairos_script_vars', array(
					'theme_location' => get_template_directory_uri(),
					'partners' => get_partners( array( 'canadian-network' ) ),
					'map_lat' => 56.049117,
					'map_lng' => -96.439453,
					'map_zoom' => 3,
					'pointer' => 'network',
				)
			);
		endif;
	}
	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array('jquery'), true );
	wp_enqueue_script( 'bootstrap-touch', get_template_directory_uri() . '/assets/js/bootstrap-touch/bootstrap-touch-carousel.js', array('jquery'), true );
	wp_enqueue_style( 'bootstrap-touch', get_template_directory_uri() . '/assets/js/bootstrap-touch/bootstrap-touch-carousel.css' );
	wp_enqueue_style( 'print', get_template_directory_uri() . '/assets/css/print.css', false, false, 'print' );

}
add_action('wp_enqueue_scripts','custom_scripts_styles');

function get_partners( $taxonomies, $children = true, $operator = "IN" ) {
	if( isset( $taxonomies) ) {
		$tax_var = array(
			'taxonomy' => 'partnership',
			'field'    => 'slug',
			'terms'    => $taxonomies,
			'include_children' => $children,
			'operator' => $operator
		);
	}
	$args = array(
		'post_type' => 'partners',
		'posts_per_page' => 1000,
		'orderby' => 'menu_order',
		'order' => 'ASC',
		'tax_query' => array( $tax_var ),
	);
	$query = new WP_Query( $args );
	if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();
	$partnerships = get_the_terms( get_the_id(), 'partnership' );
	$output[] = array(
		'id' => get_the_id(),
		'title' => the_title('','', 0 ),
		'coordinates' => get_field('coordinates'),
		'infowindow' => ( get_field('contact_name') ? get_field('contact_name') . ' - ' . antispambot( get_field('contact_email') ) : get_field('specific_location') ),
		'partnership' => reset($partnerships)->slug
	);
	endwhile; endif;
	return $output;
}

// Image Sizes
set_post_thumbnail_size( 300, 225, true );
add_image_size( 'event', 300, 225, true );
add_image_size( 'blog-featured', 400, 300, true );
add_image_size( 'blog-thumb', 300, 225, true );
add_image_size( 'm-image', 300, 225, true );
add_image_size( 'wwd-image', 300, 225, true );
add_image_size( 'event-thumb', 300, 225, true );
add_image_size( 'homepage-thumb', 300, 225, true );
if ( ! isset( $content_width ) ) {
	$content_width = 600;
}

// Menus
register_nav_menus( array(
	'primary' => 'Main Menu',
));

// Widgets
if (function_exists('register_sidebar')) {
	register_sidebar(array(
		'id' => 'main-sidebar',
		'name' => 'Blog Sidebar',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4>',
		'after_title' => '</h4>',
	));
	register_sidebar(array(
		'id' => 'category-sidebar',
		'name' => 'Category Sidebar',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4>',
		'after_title' => '</h4>',
	));
	//  register_sidebar(array(
	// 'id' => 'archive-sidebar',
	// 'name' => 'Archives/Tags Sidebar',
	// 'before_widget' => '<div id="%1$s" class="widget %2$s">',
	// 'after_widget' => '</div>',
	// 'before_title' => '<h4>',
	// 'after_title' => '</h4>',
	// ));

	register_sidebar(array(
		'id' => 'post-sidebar',
		'name' => 'Post Sidebar',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4>',
		'after_title' => '</h4>',
	));
	
	// register_sidebar(array(
	// 	'id' => 'page-body',
	// 	'name' => 'Page Body',
	// 	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	// 	'after_widget' => '</div>',
	// 	'before_title' => '<h3>',
	// 	'after_title' => '</h3>',
	// ));
	// register_sidebar(array(
	// 	'id' => 'interior-page-body',
	// 	'name' => 'Interior Page Body',
	// 	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	// 	'after_widget' => '</div>',
	// 	'before_title' => '<h3>',
	// 	'after_title' => '</h3>',
	// ));
	
	register_sidebar(array(
		'id' => 'interior-page-sidebar',
		'name' => 'Overview Menus (upper)',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<strong>',
		'after_title' => '</strong>',
	));
	register_sidebar(array(
		'id' => 'interior-page-products-sidebar',
		'name' => 'Overview Menus (lower)',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<strong>',
		'after_title' => '</strong>',
	));
	register_sidebar(array(
		'id' => 'page-sidebar',
		'name' => 'Page Sidebar',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4>',
		'after_title' => '</h4>',
	));
	register_sidebar(array(
		'id' => 'lesson-sidebar',
		'name' => 'Lesson Sidebar',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4>',
		'after_title' => '</h4>',
	));
	
	register_sidebar(array(
		'id' => 'people-sidebar',
		'name' => 'Bios Sidebar',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4>',
		'after_title' => '</h4>',
	));
	
	register_sidebar(array(
		'id' => 'shop-sidebar',
		'name' => 'Shop Sidebar',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4>',
		'after_title' => '</h4>',
	));

	


	register_sidebar(array(
		'id' => 'footer-left-widgets-area',
		'name' => 'Footer Left Widgets Area',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4>',
		'after_title' => '</h4>',
	));
	register_sidebar(array(
		'id' => 'footer-right-widgets-area',
		'name' => 'Footer Right Widgets Area',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4>',
		'after_title' => '</h4>',
	));
}


/**
 * Custom excerpt length
 */
function wp_trim_all_excerpt($text) {
	// Creates an excerpt if needed; and shortens the manual excerpt as well
	global $post;
	$raw_excerpt = $text;
	if ( '' == $text ) {
		$text = get_the_content('');
		$text = strip_shortcodes( $text );
		$text = apply_filters('the_content', $text);
		$text = str_replace(']]>', ']]&gt;', $text);
	}
	$text = strip_tags($text);
	$excerpt_length = apply_filters('excerpt_length', 55);
	$excerpt_more = apply_filters('excerpt_more', ' ' . '[...]');
	$text = wp_trim_words( $text, $excerpt_length, $excerpt_more ); //since wp3.3
	return apply_filters('wp_trim_excerpt', $text, $raw_excerpt); //since wp3.3
}

remove_filter('get_the_excerpt', 'wp_trim_excerpt');
add_filter('get_the_excerpt', 'wp_trim_all_excerpt');
function kairos_excerpt_length( $length ) {
	if( is_front_page() ) {
		return 35;
	} else{
		return 40;
	}
}
add_filter( 'excerpt_length', 'kairos_excerpt_length', 999 );

/*
 * Custom trim excerpt
 **/
function kairos_trim_excerpt($text) {
	global $post;
	return '...';
}
add_filter('excerpt_more', 'kairos_trim_excerpt');


/**
 * People post type
 */
add_action( 'init', 'register_cpt_people' );
function register_cpt_people() {
	$labels = array(
		'name' => _x( 'People', 'people' ),
		'singular_name' => _x( 'People', 'people' ),
		'add_new' => _x( 'Add New', 'People' ),
		'add_new_item' => _x( 'Add New People', 'people' ),
		'edit_item' => _x( 'Edit People', 'people' ),
		'new_item' => _x( 'New People', 'people' ),
		'view_item' => _x( 'View People', 'people' ),
		'search_items' => _x( 'Search People', 'people' ),
		'not_found' => _x( 'No People found', 'people' ),
		'not_found_in_trash' => _x( 'No People found in Trash', 'people' ),
		'parent_item_colon' => _x( 'Parent People:', 'people' ),
		'menu_name' => _x( 'People', 'people' ),
	);
	$args = array(
		'labels' => $labels,
		'hierarchical' => false,
		'supports' => array( 'title', 'editor', 'excerpt', 'thumbnail', 'custom-fields', 'revisions' ),
		'taxonomies' => array( 'Role' ),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'publicly_queryable' => true,
		'exclude_from_search' => true,
		'has_archive' => true,
		'query_var' => true,
		'can_export' => true,
		'rewrite' => array(
			'slug' => 'people',
			'with_front' => false,
			'feeds' => true,
			'pages' => true
		),
		'capability_type' => 'post'
	);
	register_post_type( 'people', $args );
}

/**
 * Role taxonomy
 */
add_action( 'init', 'register_taxonomy_roles' );
function register_taxonomy_roles() {
	$labels = array(
		'name' => _x( 'Roles', 'roles' ),
		'singular_name' => _x( 'Role', 'roles' ),
		'search_items' => _x( 'Search Roles', 'roles' ),
		'popular_items' => _x( 'Popular Roles', 'roles' ),
		'all_items' => _x( 'All Roles', 'roles' ),
		'parent_item' => _x( 'Parent Role', 'roles' ),
		'parent_item_colon' => _x( 'Parent Role:', 'roles' ),
		'edit_item' => _x( 'Edit Role', 'roles' ),
		'update_item' => _x( 'Update Role', 'roles' ),
		'add_new_item' => _x( 'Add New Role', 'roles' ),
		'new_item_name' => _x( 'New Role', 'roles' ),
		'separate_items_with_commas' => _x( 'Separate roles with commas', 'roles' ),
		'add_or_remove_items' => _x( 'Add or remove roles', 'roles' ),
		'choose_from_most_used' => _x( 'Choose from the most used roles', 'roles' ),
		'menu_name' => _x( 'Roles', 'roles' ),
	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'show_in_nav_menus' => true,
		'show_ui' => true,
		'show_tagcloud' => false,
		'show_admin_column' => false,
		'hierarchical' => true,
		'rewrite' => array(
			'slug' => 'role',
			'with_front' => false,
			'hierarchical' => true
		),
		'query_var' => true
	);
	register_taxonomy( 'roles', array('people'), $args );
}

/**
 * [people role="staff"] shortcode
 * @param  [type]   $var  [Description.]
 * @param  [string] $role Optional. [The desired taxonomy slug.]
 * @return [string]       [Returns an HTML string with the full post type could be filtered by taxonomy]
 */
function people_shortcode( $atts ) {
	$tax_var = "";
	if( isset( $atts['role'] ) ) {
		$tax_var = array(
			'taxonomy' => 'roles',
			'field'    => 'slug',
			'terms'    => $atts['role'],
		);
	}
	$args = array(
		//Type & Status Parameters
		'post_type'   => 'people',
		'orderby'   => 'menu_order',
		'order'     => 'ASC',
		'sort_id'   =>  '25574',
		'posts_per_page' => 100,
		'tax_query' => array( $tax_var ),
	);
	$query = new WP_Query( $args );
	$output = "";
	$cnt = 0;
	if ( $query->have_posts() ) :
		$output .= "<div class=\"row\">" . "\n";
		while ( $query->have_posts() ) : $query->the_post();
			$cnt++;
			$content = apply_filters( 'the_content', get_the_content() );
			$content = str_replace( ']]>', ']]&gt;', $content );
			$output .= "<div class=\"col-sm-4 col-xs-6\">" . "\n";
				/**
				 * Check if the post has post thumbnail
				 */
				if( has_post_thumbnail() ) {
					$output .= wp_get_attachment_image( get_post_thumbnail_id(), 'm-image', 0, array('title'=> '','class'	=> "img-responsive")) . "\n";
				}
				$output .= the_title( '<h2 class="bio">', '</h2>', false ) . "\n";
				if( get_field('position') ) {
					$output .= '<p><span class="position">'.get_field('position').'</span><br />' . "\n";
				}
				if( get_field('email_address') ) {
					$output .= '<span class="email-address"><a href="mailto:'.get_field('email_address').'">'.get_field('email_address').'</a></span><br />' . "\n";
				}
				if( get_field('phone_number') ) {
					$output .= '<span class="phone-number"><a href="tel:'.get_field('phone_number').'">'.get_field('phone_number').'</a></span>' . "\n";
				}
				if( get_field('extension') ) {
					$output .= '<span class="phone-extension">Ext. '.get_field('extension').'</span>' . "\n";
				}
					$output .= '</p>' . "\n";
				
				if( $content != '' ) {
					$output .= '<p><span><a data-toggle="collapse" href="#member-' . $cnt . '">Read Bio</a></span></p>' . "\n";
					$output .= '<div class="collapse" id="member-' . $cnt . '">' . "\n";
						$output .= $content . "\n";
					$output .= "</div>" . "\n";
				}
			$output .= "</div>" . "\n";
			if (($cnt % 3) == 0):
				$output .= '<div class="clearfix"></div>' . "\n";
			endif;
		endwhile;
		$output .= "</div>" . "\n";
	endif; wp_reset_postdata();
	return $output;
}
add_shortcode( 'people', 'people_shortcode' );
add_filter('next_posts_link_attributes', 'posts_link_attributes');
function posts_link_attributes() {
    return 'class="btn btn-default"';
}

/**
 * Partners Post Type
 */
add_action( 'init', 'register_cpt_partners' );
function register_cpt_partners() {
    $labels = array(
        'name' => _x( 'Partners', 'partners' ),
        'singular_name' => _x( 'Partner', 'partners' ),
        'add_new' => _x( 'Add New', 'partners' ),
        'add_new_item' => _x( 'Add New Partner', 'partners' ),
        'edit_item' => _x( 'Edit Partner', 'partners' ),
        'new_item' => _x( 'New Partner', 'partners' ),
        'view_item' => _x( 'View Partner', 'partners' ),
        'search_items' => _x( 'Search Partners', 'partners' ),
        'not_found' => _x( 'No partners found', 'partners' ),
        'not_found_in_trash' => _x( 'No partners found in Trash', 'partners' ),
        'parent_item_colon' => _x( 'Parent Partner:', 'partners' ),
        'menu_name' => _x( 'Partners', 'partners' ),
    );
    $args = array(
        'labels' => $labels,
        'hierarchical' => false,
        'supports' => array( 'title', 'editor', 'excerpt', 'custom-fields', 'revisions' ),
        'taxonomies' => array( 'continent', 'partnership' ),
        'public' => false,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => false,
        'publicly_queryable' => true,
        'exclude_from_search' => true,
        'has_archive' => false,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => false,
        'capability_type' => 'post'
    );
    register_post_type( 'partners', $args );
}

/**
 * Continent taxonomy
 */
add_action( 'init', 'register_taxonomy_continent' );
function register_taxonomy_continent() {
    $labels = array(
        'name' => _x( 'Continent', 'continent' ),
        'singular_name' => _x( 'Continents', 'continent' ),
        'search_items' => _x( 'Search Continent', 'continent' ),
        'popular_items' => _x( 'Popular Continent', 'continent' ),
        'all_items' => _x( 'All Continent', 'continent' ),
        'parent_item' => _x( 'Parent Continents', 'continent' ),
        'parent_item_colon' => _x( 'Parent Continents:', 'continent' ),
        'edit_item' => _x( 'Edit Continents', 'continent' ),
        'update_item' => _x( 'Update Continents', 'continent' ),
        'add_new_item' => _x( 'Add New Continents', 'continent' ),
        'new_item_name' => _x( 'New Continents', 'continent' ),
        'separate_items_with_commas' => _x( 'Separate continent with commas', 'continent' ),
        'add_or_remove_items' => _x( 'Add or remove continent', 'continent' ),
        'choose_from_most_used' => _x( 'Choose from the most used continent', 'continent' ),
        'menu_name' => _x( 'Continent', 'continent' ),
    );
    $args = array(
        'labels' => $labels,
        'public' => false,
        'show_in_nav_menus' => false,
        'show_ui' => true,
        'show_tagcloud' => false,
        'show_admin_column' => true,
        'hierarchical' => true,
        'rewrite' => false,
        'query_var' => true
    );
    register_taxonomy( 'continent', array('partners'), $args );
}

/**
 * Partnership taxonomy
 */
add_action( 'init', 'register_taxonomy_partnership' );
function register_taxonomy_partnership() {
    $labels = array(
        'name' => _x( 'Partnership', 'partnership' ),
        'singular_name' => _x( 'Partnerships', 'partnership' ),
        'search_items' => _x( 'Search Partnership', 'partnership' ),
        'popular_items' => _x( 'Popular Partnership', 'partnership' ),
        'all_items' => _x( 'All Partnership', 'partnership' ),
        'parent_item' => _x( 'Parent Partnerships', 'partnership' ),
        'parent_item_colon' => _x( 'Parent Partnerships:', 'partnership' ),
        'edit_item' => _x( 'Edit Partnerships', 'partnership' ),
        'update_item' => _x( 'Update Partnerships', 'partnership' ),
        'add_new_item' => _x( 'Add New Partnerships', 'partnership' ),
        'new_item_name' => _x( 'New Partnerships', 'partnership' ),
        'separate_items_with_commas' => _x( 'Separate partnership with commas', 'partnership' ),
        'add_or_remove_items' => _x( 'Add or remove partnership', 'partnership' ),
        'choose_from_most_used' => _x( 'Choose from the most used partnership', 'partnership' ),
        'menu_name' => _x( 'Partnership', 'partnership' ),
    );
    $args = array(
        'labels' => $labels,
        'public' => false,
        'show_in_nav_menus' => false,
        'show_ui' => true,
        'show_tagcloud' => false,
        'show_admin_column' => false,
        'hierarchical' => true,
        'rewrite' => false,
        'query_var' => true
    );
    register_taxonomy( 'partnership', array('partners'), $args );
}

if(function_exists('acf_add_options_page')) {
	acf_add_options_page();
}
add_filter('body_class','add_category_to_single');

function add_category_to_single($classes) {
	if (is_single() ) {
		global $post;
		foreach((get_the_category($post->ID)) as $category) {
			// add category slug to the $classes array
			$classes[] = 'category-' . $category->category_nicename;
		}
	}
	// return the $classes array
	return $classes;
}

// Customize number of archived months of blogposts
function my_limit_archives( $args ) {
    $args['limit'] = 6;
    return $args;
}
add_filter( 'widget_archives_args', 'my_limit_archives' );


//Custom widget Related Posts
class Widget_Related_Posts_Based_On_Tags extends WP_Widget {

    function __construct() {
        $widget_ops = array('classname' => 'widget_related_posts_based_on_tags', 'description' => __( "Related Posts based on Tags") );
        parent::__construct('related-posts-based-on-tags', __('Related Posts based on Tags'), $widget_ops);
        $this->alt_option_name = 'widget_related_posts_based_on_tags';

        add_action( 'save_post', array(&$this, 'flush_widget_cache') );
        add_action( 'deleted_post', array(&$this, 'flush_widget_cache') );
        add_action( 'switch_theme', array(&$this, 'flush_widget_cache') );
    }

    function widget($args, $instance) {
        $cache = wp_cache_get('widget_related_posts_based_on_tags', 'widget');

        if ( !is_array($cache) )
            $cache = array();

        if ( isset($cache[$args['widget_id']]) ) {
            echo $cache[$args['widget_id']];
            return;
        }

        ob_start();
        extract($args);

        $title = apply_filters('widget_title', empty($instance['title']) ? __('Related Posts') : $instance['title'], $instance, $this->id_base);
        if ( ! $number = absint( $instance['number'] ) )
             $number = 4;
        global $post;
        $r = new WP_Query(array(
            'posts_per_page' => $number,
            'no_found_rows' => true,
            'post_status' => 'publish',
            'ignore_sticky_posts' => true,
            'post_type' => 'post',
            'tag' => $post->post_name,));
        if ($r->have_posts()) :
?>
<?php echo $before_widget; ?>
<div class="posts-list">
    <h3><?php if ( $title ) echo $before_title . $title . $after_title; ?></h3>

<?php  while ($r->have_posts()) : $r->the_post(); ?>
<?php get_template_part( 'parts/content-post' ); ?>
<?php endwhile; ?>
</div>
<?php echo $after_widget; ?>
<?php
        // Reset the global $the_post as this query will have stomped on it
        wp_reset_postdata();

        endif;

        $cache[$args['widget_id']] = ob_get_flush();
        wp_cache_set('widget_related_posts_based_on_tags', $cache, 'widget');
    }

    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['number'] = (int) $new_instance['number'];
        $this->flush_widget_cache();

        $alloptions = wp_cache_get( 'alloptions', 'options' );
        if ( isset($alloptions['widget_related_posts_based_on_tags']) )
            delete_option('widget_related_posts_based_on_tags');

        return $instance;
    }

    function flush_widget_cache() {
        wp_cache_delete('widget_related_posts_based_on_tags', 'widget');
    }

    function form( $instance ) {
        $title = isset($instance['title']) ? esc_attr($instance['title']) : '';
        $number = isset($instance['number']) ? absint($instance['number']) : 5;

?>
<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></p>

<p><label for="<?php echo $this->get_field_id('number'); ?>"><?php _e('Number of posts to show:'); ?></label>
<input id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo $number; ?>" size="3" /></p>
<?php
    }
}

add_action('widgets_init', create_function('', 'return register_widget("Widget_Related_Posts_Based_On_Tags");'));


/* Woocommerce Customizations
 *
 *
*/
add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 28;' ), 20 );

add_filter( 'get_product_search_form' , 'woo_custom_product_searchform' );

/**
 * woo_custom_product_searchform
 *
 * @access      public
 * @since       1.0 
 * @return      void
*/
function woo_custom_product_searchform( $form ) {
	
	$form = '<form role="search" method="get" id="searchform" action="' . esc_url( home_url( '/'  ) ) . '">
		<div>
			<input type="text" value="' . get_search_query() . '" name="s" id="s" placeholder="' . __( '', 'woocommerce' ) . '" />
			<input type="submit" id="searchsubmit" value="'. esc_attr__( 'Search', 'woocommerce' ) .'" />
			<input type="hidden" name="post_type" value="product" />
		</div>
	</form>';
	
	return $form;
	
}

// Removing WooPagination
remove_action('woocommerce_pagination', 'woocommerce_pagination', 10);

function woocommerce_pagination() {
        wp_pagenavi();     
    }
add_action( 'woocommerce_pagination', 'woocommerce_pagination', 10);

 
 
/** Remove Showing results functionality site-wide */
function woocommerce_result_count() {
        return;
}

// Hides the 'Free!' price notice
add_filter( 'woocommerce_variable_free_price_html',  'hide_free_price_notice' );
 
add_filter( 'woocommerce_free_price_html',           'hide_free_price_notice' );
 
add_filter( 'woocommerce_variation_free_price_html', 'hide_free_price_notice' );
 
function hide_free_price_notice( $price ) {
 
	return 'No charge but donations welcome.';
}

// Remove product thumbnail from the cart page
add_filter( 'woocommerce_cart_item_thumbnail', '__return_empty_string' );

?>
