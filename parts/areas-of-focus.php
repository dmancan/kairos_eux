	<?php

	$args = array(
		//Type & Status Parameters
		'post_type'   => 'aof',
		'posts_per_page' => 6,
		'orderby'   => 'menu_order',
	);
	
	$query = new WP_Query( $args );
	$rowCounter = 0;
	
	if ( $query->have_posts() ) : $counter = 1; ?>
		<div class="row">
		<?php while ( $query->have_posts() ) : $query->the_post(); ?>
			
			<?php if ($rowCounter == 0):?>
				
			<?php endif; ?>
				
				<div class="col-sm-6 col-md-4">
					<div class="well" style="border-radius: 1px;">
					<a href="<?php the_permalink(); ?>" class="item-more"><?php if(has_post_thumbnail()){ the_post_thumbnail('wwd-image',array( 'class'	=> "img-responsive")); } ?>
						<h2 style="margin-top: -10px;"><?php the_title(); ?></h2></a>
						<?php the_excerpt(); ?>
					</div>
				</div>
				
			<?php 
				$rowCounter++;
				if ($rowCounter == 2):?>
					<div class="clearfix visible-xs visible-sm"></div>
				
			<?php endif; ?>
			<?php 
				if ($rowCounter == 3):?>
					<div class="clearfix visible-md visible-lg"></div>
				<?php $rowCounter = 0; ?>
			<?php endif; ?>
		<?php endwhile; ?>
		</div>
		<?php 
			// close row
			if ($rowCounter < 3):?>
			
			<?php $rowCounter = 0; ?>
		<?php endif; ?>
	<?php endif; wp_reset_postdata(); ?>