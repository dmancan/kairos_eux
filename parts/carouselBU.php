	<?php 
		// bootstrap carousel
		$firstActive = true;
		$cnt = 0;
		if (get_field('_carousel')): ?>

	
		
		<section class="carousel-wrapper">
			<div id="carousel" class="carousel slide" data-ride="carousel">
				<ol class="carousel-indicators">
					<?php 
					 while(has_sub_field('_carousel')):?>	
					<li data-target="#carousel" data-slide-to="<?php echo $cnt; ?>"
						<?php if ($firstActive) : ?>
							class="active"
						<?php endif; $firstActive = false; $cnt++; ?>
					></li>
					<?php endwhile;?>
				</ol>
				<div class="carousel-inner" role="listbox">
					<?php 
						$firstActive = true;
						while(has_sub_field("_carousel")):?>
					<div class="item 
						<?php if ($firstActive) : ?>
							active
						<?php endif; $firstActive = false; ?>
						">
						<?php if(get_sub_field('_carousel_img')):?>
							<div class="bg" style="background-image: url(<?php echo wp_get_attachment_image_src(get_sub_field('_carousel_img'), 'full')[0]; ?>);"></div>
						<?php endif;?>
						<div class="carousel-caption">
							<?php if(get_sub_field('_carousel_title')) echo '<h1 class="hidden-xs">'.get_sub_field('_carousel_title').'</h1>'?>
							<?php if(get_sub_field('_carousel_text')) echo '<h2 class="hidden-xs" style="margin:0;">'.get_sub_field('_carousel_text').'</h2>'?>
							<?php if(get_sub_field('_carousel_btn')) echo '<p class="hidden-xs"><a href="'.get_sub_field('_carousel_btn').'" class="btn btn-default">'.get_sub_field('_carousel_btn_name').'</a></p>'?>
							<?php if(get_sub_field('_carousel_btn')) {
								echo '<h1 class="visible-xs"><a href="'.get_sub_field('_carousel_btn').'">'.get_sub_field('_carousel_title').'</a></h1>';
								} else {
								echo '<h1 class="visible-xs">'.get_sub_field('_carousel_btn_name').'</h1>';
								}
							?>
							<?php //if(get_sub_field('_sl_btn')) echo '<p class="visible-xs">'.get_sub_field('_sl_text').'</p>'?>

						</div>
		    		</div>
					<?php endwhile;?>
				</div>
			</div>
		</section>
	
	<?php endif;?>