	<?php 
		// bootstrap carousel
		$firstActive = true;
		$cnt = 0;
		if (get_field('slider')): ?>
		
		<section class="carousel-wrapper">
			<div id="carousel" class="carousel slide" data-ride="carousel">
				<ol class="carousel-indicators">
					<?php 
					 while(has_sub_field('slider')):?>	
					<li data-target="#carousel" data-slide-to="<?php echo $cnt; ?>"
						<?php if ($firstActive) : ?>
							class="active"
						<?php endif; $firstActive = false; $cnt++; ?>
					></li>
					<?php endwhile;?>
				</ol>
				<div class="carousel-inner" role="listbox">
					<?php 
						$firstActive = true;
						while(has_sub_field("slider")):?>
					<div class="item 
						<?php if ($firstActive) : ?>
							active
						<?php endif; $firstActive = false; ?>
						">
						<?php if(get_sub_field('slider_image')):?>
							<div class="bg" style="background-image: url(<?php echo wp_get_attachment_image_src(get_sub_field('slider_image'), 'full')[0]; ?>);"></div>
						<?php endif;?>
						<div class="carousel-caption">
							<?php if(get_sub_field('slider_text')) echo '<div class="contentarea">'.get_sub_field('slider_text').'</div>'?>
						</div>
						<div class="carousel-button"><?php if(get_sub_field('button_text')) echo '<p><a href="'.get_sub_field('slider_url').'" class="btn btn-default">'.get_sub_field('button_text').'</a></p>'?>
						
						</div>
		    		</div>
					<?php endwhile;?>
				</div>
			</div>
		</section>
	
	<?php endif;?>