					<div class="col-md-3">
						<?php if( has_post_thumbnail() ): ?>
						<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('blog-thumb', array( 'class'	=> "img-responsive"));?></a>
						<?php endif; ?>
						<span class="date"><?php the_time('F j, Y'); ?></span>
						<p><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
					</div>