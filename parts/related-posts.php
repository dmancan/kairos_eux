
		<?php $args_category = get_field('category');?>
		<?php if(!empty($args_category)):?>
			<?php $recent_post = new WP_Query(array(
				'post_type' => 'post',
				'showposts' => 4,
				'category__in' => $args_category,
			));?>
			<?php if($recent_post->have_posts()):?>
				<hr class="hidden-xs">
				<h3>Related Posts | <small><a href="<?php echo get_category_link($args_category[0]);?>">Show all</a></small></h3>
				<div class="row">
					<?php while($recent_post->have_posts()): $recent_post->the_post();?>
						<?php get_template_part( 'parts/related-posts-post'); ?>
					<?php endwhile;?>
				</div>
			
			<?php endif; wp_reset_postdata();?>
		<?php endif;?>
		
		<?php $args_tags = get_field('tags');?>
		<?php if(!empty($args_tags)):?>
			<?php $recent_post = new WP_Query(array(
				'post_type' => 'post',
				'showposts' => 4,
				'tag__in' => $args_tags,
			));?>
			<?php if($recent_post->have_posts()):?>
					<hr class="hidden-xs">
					<h3>Related Posts | <small><a href="<?php echo get_tag_link($args_tags[0]);?>">Show all</a></small></h3>
					<div class="row">
						<?php while($recent_post->have_posts()): $recent_post->the_post();?>
							<?php get_template_part( 'parts/related-posts-post'); ?>
						<?php endwhile;?>
					</div>
	
			<?php endif; wp_reset_postdata();?>
		<?php endif;?>