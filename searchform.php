<form method="get" id="searchform" class="form-inline" action="<?php echo home_url(); ?>/">
	<div class="form-group">
		<input type="text" value="<?php the_search_query(); ?>" name="s" id="s" size="40" class="form-control input-lg" />
	</div>
	<button type="submit" id="searchsubmit" class="btn btn-primary">Search</button>
</form>